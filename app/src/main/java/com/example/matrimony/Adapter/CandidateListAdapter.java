package com.example.matrimony.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.matrimony.R;
import com.example.matrimony.database.TblCandidate;
import com.example.matrimony.models.candidateModel;

import java.util.ArrayList;

public class CandidateListAdapter extends RecyclerView.Adapter<CandidateListAdapter.ViewHolder> {

    ArrayList<candidateModel> candidateList;

    Context context;

    TblCandidate tblCandidate;

    public CandidateListAdapter(ArrayList<candidateModel> candidateList, Context context) {
        this.candidateList = candidateList;
        this.context = context;
        tblCandidate = new TblCandidate(context);
    }

    @NonNull
    @Override
    public CandidateListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_candidate_list,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CandidateListAdapter.ViewHolder holder, int position) {

        holder.row_candidate_list_tvName.setText(candidateList.get(position).getName());

        holder.row_candidate_list_tvEmail.setText(candidateList.get(position).getEmail());

        holder.row_candidate_list_tvDOB.setText(candidateList.get(position).getDob());

        if(candidateList.get(position).getIsFavorite()==0){
            holder.row_candidate_list_ivFavoriteCandidate.setImageResource(R.drawable.ic_unfavorite);
            }
        else{
            holder.row_candidate_list_ivFavoriteCandidate.setImageResource(R.drawable.ic_favorite);
        }

        holder.row_candidate_list_ivFavoriteCandidate.setOnClickListener(view -> {
            tblCandidate.changeFavorite(candidateList.get(position).getCandidateId(),candidateList.get(position).getIsFavorite() == 0 ? 1 : 0);
            candidateList.get(position).setIsFavorite(candidateList.get(position).getIsFavorite() == 0 ? 1 : 0);
            notifyDataSetChanged();
        });

        holder.row_candidate_list_ivDeleteCandidate.setOnClickListener(view -> {

            tblCandidate.deleteCandidateRecord(candidateList.get(position).getCandidateId());

            candidateList.remove(candidateList.get(position));

            notifyItemRemoved(position);
            notifyDataSetChanged();


        });

    }

    @Override
    public int getItemCount() {
        return candidateList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView row_candidate_list_tvName,row_candidate_list_tvEmail,row_candidate_list_tvDOB,activity_list_candidate_tvNoDataFound;
        ImageView row_candidate_list_ivFavoriteCandidate,row_candidate_list_ivDeleteCandidate;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            row_candidate_list_tvName = itemView.findViewById(R.id.row_candidate_list_tvName);
            row_candidate_list_tvEmail = itemView.findViewById(R.id.row_candidate_list_tvEmail);
            row_candidate_list_tvDOB = itemView.findViewById(R.id.row_candidate_list_tvDOB);
            activity_list_candidate_tvNoDataFound = itemView.findViewById(R.id.activity_list_candidate_tvNoDataFound);

            row_candidate_list_ivFavoriteCandidate = itemView.findViewById(R.id.row_candidate_list_ivFavoriteCandidate);
            row_candidate_list_ivDeleteCandidate = itemView.findViewById(R.id.row_candidate_list_ivDeleteCandidate);
        }
    }
}
