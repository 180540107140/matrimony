package com.example.matrimony.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.matrimony.models.candidateModel;

import java.util.ArrayList;

public class TblCandidate extends DatabaseHelper{

    //Table Name
    public static final String TBL_CANDIDATE = "TblCandidate";

    //Column Names
    public static final String COL_TBL_CANDIDATE_CANDIDATEID = "CandidateId";
    public static final String COL_TBL_CANDIDATE_NAME = "Name";
    public static final String COL_TBL_CANDIDATE_FATHERNAME = "FatherName";
    public static final String COL_TBL_CANDIDATE_SURNAME = "SurName";
    public static final String COL_TBL_CANDIDATE_GENDER = "Gender";

    public static final String COL_TBL_CANDIDATE_EMAIL = "Email";
    public static final String COL_TBL_CANDIDATE_MOBILE = "Mobile";
    public static final String COL_TBL_CANDIDATE_HOBBIES = "Hobbies";
    public static final String COL_TBL_CANDIDATE_ISFAVORITE = "IsFavorite";
    public static final String COL_TBL_CANDIDATE_DOB = "DOB";

    //Select For db.rawQuery
    //Update For db.update
    //Insert For db.insert
    //Delete For db.delete

    public TblCandidate(Context context) {
        super(context);
    }

    //Insert Into Database
    public  long insertCandidateRecord(candidateModel candidateModel){

        //Insert / Update / Modify / Delete That Use getWritableDatabase()
        //Select / View That Use getReadableDatabase()

        //Sqlite Database Object
        SQLiteDatabase db = getWritableDatabase();

        //Content Values Blank
        ContentValues cv = new ContentValues();

        cv.put(COL_TBL_CANDIDATE_NAME,candidateModel.getName());
        cv.put(COL_TBL_CANDIDATE_FATHERNAME,candidateModel.getFatherName());
        cv.put(COL_TBL_CANDIDATE_SURNAME,candidateModel.getSurName());
        cv.put(COL_TBL_CANDIDATE_GENDER,candidateModel.getGender());
        cv.put(COL_TBL_CANDIDATE_EMAIL,candidateModel.getEmail());

        cv.put(COL_TBL_CANDIDATE_HOBBIES,candidateModel.getHobbies());
        cv.put(COL_TBL_CANDIDATE_ISFAVORITE,candidateModel.getIsFavorite());
        cv.put(COL_TBL_CANDIDATE_DOB,candidateModel.getDob());
        cv.put(COL_TBL_CANDIDATE_MOBILE,candidateModel.getMobile());

        //Insert Record Into Database

        //long update = db.update(TBL_CANDIDATE,cv,""+COL_TBL_CANDIDATE_CANDIDATEID+" = ?",new String[]{String.valueOf(candidateModel.getCandidateId())});

        //Return If Inserted 1 Otherwise 0 Or Less
        return db.insert(TBL_CANDIDATE,null,cv);

    }

    //Select TblCandidate Table
    public ArrayList<candidateModel> all_candidate_list(){

        //SQLite Database Object
        SQLiteDatabase db = getWritableDatabase();

        //Create Array List Empty
        ArrayList<candidateModel> candidateList = new ArrayList<>();

        //Sql Statement
        String query = "select * from "+TBL_CANDIDATE;

        //Get Data Into Cursor
        Cursor cursor = db.rawQuery(query,null);

        //Reset Position Of Cursor
        cursor.moveToFirst();

        if (cursor.getCount()>0) {
            do{

                //CandidateModel Empty
                candidateModel candidateModel = new candidateModel();

                //Set Data Into Model
                candidateModel.setCandidateId(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_CANDIDATEID)));
                candidateModel.setName(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_NAME)));
                candidateModel.setSurName(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_SURNAME)));
                candidateModel.setFatherName(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_FATHERNAME)));
                candidateModel.setGender(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_GENDER)));

                candidateModel.setEmail(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_EMAIL)));
                candidateModel.setMobile(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_MOBILE)));
                candidateModel.setHobbies(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_HOBBIES)));
                candidateModel.setIsFavorite(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_ISFAVORITE)));
                candidateModel.setDob(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_DOB)));

                //Candidate Model Add Into List
                candidateList.add(candidateModel);

            }while (cursor.moveToNext());
        }

        //Database Close
        db.close();

        //Cursor Close
        cursor.close();

        //Return Candidate List

        return candidateList;
    }

    public void changeFavorite(int candidateId,int favoriteVAlue){
        SQLiteDatabase db = getWritableDatabase();

         String query = " Update "+TBL_CANDIDATE+" SET "+COL_TBL_CANDIDATE_ISFAVORITE+" = "+favoriteVAlue+" where "+COL_TBL_CANDIDATE_CANDIDATEID+" = "+candidateId;

        db.execSQL(query);

        db.close();
    }

    public void deleteCandidateRecord(int candidateId){

        SQLiteDatabase db = getWritableDatabase();

        String query = "delete from "+TBL_CANDIDATE+" where "+COL_TBL_CANDIDATE_CANDIDATEID+" = "+candidateId;

        db.execSQL(query);

        db.close();
    }

    //Select TblCandidate Table for favorite candidate
    public ArrayList<candidateModel> all_candidate_favorite_list(){

        //SQLite Database Object
        SQLiteDatabase db = getWritableDatabase();

        //Create Array List Empty
        ArrayList<candidateModel> candidateList = new ArrayList<>();

        //Sql Statement
        String query = "select * from "+TBL_CANDIDATE+" where "+COL_TBL_CANDIDATE_ISFAVORITE+" = 1";

        //Get Data Into Cursor
        Cursor cursor = db.rawQuery(query,null);

        //Reset Position Of Cursor
        cursor.moveToFirst();

       if (cursor.getCount()>0) {
           do {

               //CandidateModel Empty
               candidateModel candidateModel = new candidateModel();

               //Set Data Into Model
               candidateModel.setCandidateId(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_CANDIDATEID)));
               candidateModel.setName(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_NAME)));
               candidateModel.setSurName(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_SURNAME)));
               candidateModel.setFatherName(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_FATHERNAME)));
               candidateModel.setGender(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_GENDER)));

               candidateModel.setEmail(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_EMAIL)));
               candidateModel.setMobile(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_MOBILE)));
               candidateModel.setHobbies(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_HOBBIES)));
               candidateModel.setIsFavorite(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_ISFAVORITE)));
               candidateModel.setDob(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_DOB)));

               //Candidate Model Add Into List
               candidateList.add(candidateModel);

           } while (cursor.moveToNext());
       }
        //Database Close
        db.close();

        //Cursor Close
        cursor.close();

        //Return Candidate List

        return candidateList;
    }

    public ArrayList<candidateModel> all_search_list(String data){

        //SQLite Database Object
        SQLiteDatabase db = getWritableDatabase();

        //Create Array List Empty
        ArrayList<candidateModel> candidateList = new ArrayList<>();

        //Sql Statement
        String query = "select * from "+TBL_CANDIDATE+" where "+COL_TBL_CANDIDATE_NAME+" LIKE '%"+data+" %'";

        //Get Data Into Cursor
        Cursor cursor = db.rawQuery(query,null);

        //Reset Position Of Cursor
        cursor.moveToFirst();

        if (cursor.getCount()>0) {
            do{

                //CandidateModel Empty
                candidateModel candidateModel = new candidateModel();

                //Set Data Into Model
                candidateModel.setCandidateId(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_CANDIDATEID)));
                candidateModel.setName(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_NAME)));
                candidateModel.setSurName(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_SURNAME)));
                candidateModel.setFatherName(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_FATHERNAME)));
                candidateModel.setGender(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_GENDER)));

                candidateModel.setEmail(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_EMAIL)));
                candidateModel.setMobile(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_MOBILE)));
                candidateModel.setHobbies(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_HOBBIES)));
                candidateModel.setIsFavorite(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_ISFAVORITE)));
                candidateModel.setDob(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_DOB)));

                //Candidate Model Add Into List
                candidateList.add(candidateModel);

            }while (cursor.moveToNext());
        }

        //Database Close
        db.close();

        //Cursor Close
        cursor.close();

        //Return Candidate List

        return candidateList;
    }
}
