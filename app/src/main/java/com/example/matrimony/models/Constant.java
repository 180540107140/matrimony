package com.example.matrimony.models;

public class Constant {

    public final static int MALE = 1;
    public final static int FEMALE = 2;

    public final static int ISFAVORITEDEFAULT = 0;
}
