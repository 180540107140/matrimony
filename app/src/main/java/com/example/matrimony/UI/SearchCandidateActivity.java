package com.example.matrimony.UI;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.matrimony.Adapter.CandidateListAdapter;
import com.example.matrimony.R;
import com.example.matrimony.database.TblCandidate;
import com.example.matrimony.models.candidateModel;

import java.util.ArrayList;

public class SearchCandidateActivity extends AppCompatActivity {

    ArrayList<candidateModel> candidateList;

    TblCandidate tblCandidate;

    CandidateListAdapter adapter;

    TextView activity_search_tvNoDataFound;

    RecyclerView activity_search_rvSearchList;

    EditText activity_search_etSearch;

    String data;

    @Override
    protected void onCreate(Bundle savedInstenceState) {
        super.onCreate(savedInstenceState);
        setContentView(R.layout.activity_search);

        init();
        process();
        listeners();
    }



    private void init() {

        activity_search_etSearch = findViewById(R.id.activity_search_etSearch);

        activity_search_rvSearchList = findViewById(R.id.activity_search_rvSearchList);

        activity_search_tvNoDataFound = findViewById(R.id.activity_search_tvNoDataFound);

        candidateList = new ArrayList<>();

        tblCandidate = new TblCandidate(this);

        candidateList = tblCandidate.all_candidate_favorite_list();


    }

    private void process() {

        checkDataIsEmpty();

    }
    private void listeners() {

        activity_search_etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {

                data = s.toString();
                getDataFromDatabase();

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    void setAdapter(){
        activity_search_rvSearchList.setLayoutManager(new GridLayoutManager(this,1));

        adapter = new CandidateListAdapter(candidateList,this);

        activity_search_rvSearchList.setAdapter(adapter);

    }

    private void getDataFromDatabase(){

        candidateList = tblCandidate.all_search_list(data);
        checkDataIsEmpty();
    }

    void checkDataIsEmpty(){

        if(candidateList.size()<1){

            activity_search_tvNoDataFound.setVisibility(View.VISIBLE);
            activity_search_rvSearchList.setVisibility(View.GONE);
        }else {

            activity_search_tvNoDataFound.setVisibility(View.GONE);
            activity_search_rvSearchList.setVisibility(View.VISIBLE);

            setAdapter();

        }
    }


}
