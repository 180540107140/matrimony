package com.example.matrimony.UI;

import android.content.Intent;
import android.os.Bundle;


import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.example.matrimony.R;


public class MainActivity extends AppCompatActivity {

    Intent intent;
    CardView dash_addcandidate, dash_listcandidate, dash_searchcandidate, dash_favcandidate;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        init();
        process();
        listners();
    }

    private void init() {
        dash_addcandidate = findViewById(R.id.dash_addcandidate);
        dash_listcandidate = findViewById(R.id.dash_listcandidate);
        dash_searchcandidate = findViewById(R.id.dash_searchcandidate);
        dash_favcandidate = findViewById(R.id.dash_favcandidate);
    }

    private void process() {

    }

    private void listners() {
        dash_addcandidate.setOnClickListener(v -> {
            intent = new Intent( MainActivity.this, AddUserActivity.class);
            startActivity(intent);
        });

        dash_listcandidate.setOnClickListener(v -> {
            intent = new Intent( MainActivity.this, ListCandidateActivity.class);
            startActivity(intent);
        });

        dash_searchcandidate.setOnClickListener(v -> {
            intent = new Intent( MainActivity.this, SearchCandidateActivity.class);
            startActivity(intent);
        });

        dash_favcandidate.setOnClickListener(v -> {
            intent = new Intent( MainActivity.this, FavCandidateActivity.class);
            startActivity(intent);
        });
    }


}