package com.example.matrimony.UI;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.matrimony.Adapter.CandidateListAdapter;
import com.example.matrimony.R;
import com.example.matrimony.database.TblCandidate;
import com.example.matrimony.models.candidateModel;

import java.util.ArrayList;

public class FavCandidateActivity extends AppCompatActivity {

    ArrayList<candidateModel> candidatelist;

    TblCandidate tblCandidate;

    CandidateListAdapter adapter;

    TextView activity_favorite_tvNoDataFound;

    RecyclerView activity_favorite_rvFavoriteList;
    @Override
    protected void onCreate(Bundle savedInstenceState){
        super.onCreate(savedInstenceState);
        setContentView(R.layout.activity_favorite);

        init();
        process();
        listeners();
    }

    private void init() {

        candidatelist = new ArrayList<>();

        tblCandidate = new TblCandidate(this);

        candidatelist = tblCandidate.all_candidate_favorite_list();

        activity_favorite_tvNoDataFound = findViewById(R.id.activity_favorite_tvNoDataFound);

        activity_favorite_rvFavoriteList = findViewById(R.id.activity_favorite_rvFavoriteList);

    }
    private void process() {

        if(candidatelist.size()<1){

            activity_favorite_tvNoDataFound.setVisibility(View.VISIBLE);
            activity_favorite_rvFavoriteList.setVisibility(View.GONE);
        }else{

            activity_favorite_tvNoDataFound.setVisibility(View.GONE);
            activity_favorite_rvFavoriteList.setVisibility(View.VISIBLE);

            activity_favorite_rvFavoriteList.setLayoutManager(new GridLayoutManager(this,1));

            adapter = new CandidateListAdapter(candidatelist,this);

            activity_favorite_rvFavoriteList.setAdapter(adapter);

        }

    }
    private void listeners() {
    }
}
