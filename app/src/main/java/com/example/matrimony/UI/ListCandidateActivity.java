package com.example.matrimony.UI;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.matrimony.Adapter.CandidateListAdapter;
import com.example.matrimony.R;
import com.example.matrimony.database.TblCandidate;
import com.example.matrimony.models.candidateModel;

import java.util.ArrayList;

public class ListCandidateActivity extends AppCompatActivity {


    ArrayList<candidateModel> candidatelist;



    TblCandidate tblCandidate;

    CandidateListAdapter adapter;

    RecyclerView activity_list_candidate_rvCandidateList;

    TextView activity_list_candidate_tvNoDataFound;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_candidate);

        init();
        process();
        listeners();
    }

    private void init() {

        candidatelist = new ArrayList<>();

        tblCandidate = new TblCandidate(this);

        candidatelist = tblCandidate.all_candidate_list();

        activity_list_candidate_rvCandidateList = findViewById(R.id.activity_list_candidate_rvCandidateList);

        activity_list_candidate_tvNoDataFound = findViewById(R.id.activity_list_candidate_tvNoDataFound);
    }

    private void process() {

        if(candidatelist.size()<1){

            activity_list_candidate_tvNoDataFound.setVisibility(View.VISIBLE);
            activity_list_candidate_rvCandidateList.setVisibility(View.GONE);

        }else{

            activity_list_candidate_tvNoDataFound.setVisibility(View.GONE);
            activity_list_candidate_rvCandidateList.setVisibility(View.VISIBLE);

            activity_list_candidate_rvCandidateList.setLayoutManager(new GridLayoutManager(this,1));

            adapter = new CandidateListAdapter(candidatelist,this);

            activity_list_candidate_rvCandidateList.setAdapter(adapter);
        }
    }

    private void listeners() {

    }


}
